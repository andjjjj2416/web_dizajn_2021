var navLinks = document.getElementById("navLinks");

function showMenu() {
  navLinks.style.right = "0";
}

function hideMenu() {
  navLinks.style.right = "-200px";
}

function otvorilogin() {
  document.getElementById("mojaForma").style.display = "block";
}

function zatvorilogin() {
  document.getElementById("mojaForma").style.display = "none";
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
