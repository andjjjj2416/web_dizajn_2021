let ulogujse = document.getElementById("ulogujse");
let loginForm = document.getElementById("loginform");
ulogujse.addEventListener("click", function (e) {
  let email = document.getElementById("email_korisnika").value.trim();
  let password = document.getElementById("psw").value.trim();
  console.log(email);
  console.log(password);

  /*let validno = true;*/
  if (email == "" || password == "") {
    alert("Unesite sve podatke");
    /*validno = false;*/
  } else {
    let request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          console.log(this.responseText);
          let users = JSON.parse(request.responseText);
          let name = "";
          for (let id in users) {
            let user = users[id];
            console.log(user);
            if (user.email == email && user.lozinka == password) {
              name = user.ime;
              break;
            }
          }
          if (name == "") {
            alert("Neispravni login podaci");
          } else {
            alert("Uspesno ste se ulogovali");
          }
        } else {
          alert("Greska " + this.statusText);
        }
      }
    };
    let url = loginForm.getAttribute("action");
    request.open("GET", url);
    request.send();
  }
});
